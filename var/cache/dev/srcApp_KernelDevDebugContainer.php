<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerEdf6lJI\srcApp_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerEdf6lJI/srcApp_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerEdf6lJI.legacy');

    return;
}

if (!\class_exists(srcApp_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerEdf6lJI\srcApp_KernelDevDebugContainer::class, srcApp_KernelDevDebugContainer::class, false);
}

return new \ContainerEdf6lJI\srcApp_KernelDevDebugContainer(array(
    'container.build_hash' => 'Edf6lJI',
    'container.build_id' => 'c53edb9f',
    'container.build_time' => 1547122051,
), __DIR__.\DIRECTORY_SEPARATOR.'ContainerEdf6lJI');
