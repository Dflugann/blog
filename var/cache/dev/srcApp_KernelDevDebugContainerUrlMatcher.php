<?php

use Symfony\Component\Routing\Matcher\Dumper\PhpMatcherTrait;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    use PhpMatcherTrait;

    public function __construct(RequestContext $context)
    {
        $this->context = $context;
        $this->staticRoutes = array(
            '/admin/posts' => array(array(array('_route' => 'admin_post', '_controller' => 'App\\Controller\\Admin\\AdminPostController::index'), null, null, null, true, null)),
            '/admin/posts/form' => array(array(array('_route' => 'admin_post_form', '_controller' => 'App\\Controller\\Admin\\AdminPostController::form'), null, null, null, false, null)),
            '/admin/users' => array(array(array('_route' => 'admin_users', '_controller' => 'App\\Controller\\Admin\\AdminUserController::index'), null, null, null, true, null)),
            '/admin/users/new' => array(array(array('_route' => 'admin_user_new', '_controller' => 'App\\Controller\\Admin\\AdminUserController::form'), null, null, null, false, null)),
            '/admin/category' => array(array(array('_route' => 'admin_category', '_controller' => 'App\\Controller\\Admin\\CategoryController::index'), null, null, null, true, null)),
            '/admin/category/form' => array(array(array('_route' => 'admin_category_form', '_controller' => 'App\\Controller\\Admin\\CategoryController::formCategory'), null, null, null, false, null)),
            '/' => array(array(array('_route' => 'home', '_controller' => 'App\\Controller\\HomeController::index'), null, null, null, false, null)),
            '/_profiler' => array(array(array('_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'), null, null, null, true, null)),
            '/_profiler/search' => array(array(array('_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'), null, null, null, false, null)),
            '/_profiler/search_bar' => array(array(array('_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'), null, null, null, false, null)),
            '/_profiler/phpinfo' => array(array(array('_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'), null, null, null, false, null)),
            '/_profiler/open' => array(array(array('_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'), null, null, null, false, null)),
        );
        $this->regexpList = array(
            0 => '{^(?'
                    .'|/admin/(?'
                        .'|posts/(?'
                            .'|update/([^/]++)(*:41)'
                            .'|delete/([^/]++)(*:63)'
                        .')'
                        .'|users/(?'
                            .'|update/([^/]++)(*:95)'
                            .'|delete/([^/]++)(*:117)'
                        .')'
                        .'|category/(?'
                            .'|update/([^/]++)(*:153)'
                            .'|delete/([^/]++)(*:176)'
                        .')'
                    .')'
                    .'|/_(?'
                        .'|error/(\\d+)(?:\\.([^/]++))?(*:217)'
                        .'|wdt/([^/]++)(*:237)'
                        .'|profiler/([^/]++)(?'
                            .'|/(?'
                                .'|search/results(*:283)'
                                .'|router(*:297)'
                                .'|exception(?'
                                    .'|(*:317)'
                                    .'|\\.css(*:330)'
                                .')'
                            .')'
                            .'|(*:340)'
                        .')'
                    .')'
                .')(?:/?)$}sDu',
        );
        $this->dynamicRoutes = array(
            41 => array(array(array('_route' => 'admin_post_update', '_controller' => 'App\\Controller\\Admin\\AdminPostController::update'), array('id'), null, null, false, null)),
            63 => array(array(array('_route' => 'admin_post_delete', '_controller' => 'App\\Controller\\Admin\\AdminPostController::delete'), array('id'), null, null, false, null)),
            95 => array(array(array('_route' => 'admin_user_update', '_controller' => 'App\\Controller\\Admin\\AdminUserController::update'), array('id'), null, null, false, null)),
            117 => array(array(array('_route' => 'admin_user_delete', '_controller' => 'App\\Controller\\Admin\\AdminUserController::delete'), array('id'), null, null, false, null)),
            153 => array(array(array('_route' => 'admin_category_update', '_controller' => 'App\\Controller\\Admin\\CategoryController::update'), array('id'), null, null, false, null)),
            176 => array(array(array('_route' => 'admin_category_delete', '_controller' => 'App\\Controller\\Admin\\CategoryController::delete'), array('id'), null, null, false, null)),
            217 => array(array(array('_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'), array('code', '_format'), null, null, false, null)),
            237 => array(array(array('_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'), array('token'), null, null, false, null)),
            283 => array(array(array('_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'), array('token'), null, null, false, null)),
            297 => array(array(array('_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'), array('token'), null, null, false, null)),
            317 => array(array(array('_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'), array('token'), null, null, false, null)),
            330 => array(array(array('_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'), array('token'), null, null, false, null)),
            340 => array(array(array('_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'), array('token'), null, null, false, null)),
        );
    }
}
